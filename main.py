from Directory import Directory


root = Directory('/')
pwd = [root]

print('<Starting your application...>')

while True:
	command = input('$: ')

	cmd = command.split()


	#Present working directory is always preserved in the `pwd` array. `pwd` is a list of the directories indicating the current path
	if cmd[0] == 'pwd':
		if len(cmd) > 1:
			print('TOO MANY ARGUMENTS')
		else:
			print('PATH: /' + '/'.join(x.dir_name for x in pwd[1:]))

	elif cmd[0] == 'mkdir':

		dirs = cmd[1].split('/')


		#Very basic edge case handling
		if cmd[1] == '/':
			print('ERR: INVALID DIRECTORY')

		#If the second arg starts with a `/`, that means the path is an absolute path not relative
		elif cmd[1][0] == '/':
			present = root
			dirs = dirs[1:]
		else:
			present = pwd[-1]

		
		#For cases where only one directory is to be traversed, there is no need for traversal
		if len(dirs) == 1:

			flag = 0
			for index, child in enumerate(present.children):
				if child.dir_name == dirs[0]:
					flag = 1
					print('ERR: DIRECTORY ALREADY EXISTS')
					break
			
			if flag == 0:
				new_directory = Directory(dirs[-1])
				present.children.append(new_directory)
				print('SUCC: CREATED')
					

		else:
			# Cases where there are multiple directories to be traversed before performing the operation
			# for example `cd Desktop/Playment`
			# in the above example, the system should first traverse to Desktop and then Playment folder
			node = present
			count = 0
			

			if dirs[0] == '':
				dirs = dirs[1:]


			for d in dirs:

				found_in_child = False

				for index, child in enumerate(node.children):


					#Directory traversal
					if child.dir_name == d:
						found_in_child = True
						count += 1
						curr = node
						node = child


					if found_in_child and count == (len(dirs)-1):
						flag = 0
						for index, child in enumerate(node.children):
							if child.dir_name == dirs[-1]:
								flag = 1
								print('ERR: DIRECTORY ALREADY EXISTS')


						if flag == 0:
							new_directory = Directory(dirs[-1])
							node.children.append(new_directory)
							print("SUCC: CREATED")



	elif cmd[0] == 'ls':

		

		if len(cmd) > 1:

			dirs = cmd[1].split('/')

			#Some corner cases handled
			if cmd[1][0] == '/':
				present = root
			else:
				present = pwd[-1]

			if cmd[1] == '/':
				print('DIRS: ', " ".join([x.dir_name for x in present.children]))

			elif len(dirs) == 1:

				flag = 0
				for index, child in enumerate(present.children):
					if child.dir_name == dirs[0]:
						flag = 1
						print('DIRS: ', " ".join([x.dir_name for x in child.children]))
						break
				
				if flag == 0:
					print('ERR: INVALID PATH')

			else:
				#Directory traversal in case of multiple directories like `ls naman/playment`
				if cmd[1][0] == '/':
					dirs = dirs[1:]


				node = present
				count = 0

				for d in dirs:
					found_in_child = False

					flag = 0

					for index, child in enumerate(node.children):

						if child.dir_name == d:
							found_in_child = True
							count += 1
							curr = node
							node = child
							flag = 1


						if found_in_child and count == (len(dirs)):

							print('DIRS: ', " ".join([x.dir_name for x in node.children]))

				if flag == 0:

					print('ERR: INVALID PATH')
		
		else:
			print('DIRS: ', " ".join([x.dir_name for x in pwd[-1].children]))

	elif cmd[0] == 'cd':
		#cd changes the current working directory

		if cmd[1][0] == '/':
			pwd = [root]

		#Directory traversal
		dirs = cmd[1].split('/')
		for i in dirs:
			flag = 0
			if i == '':
				continue
			if i == '..' and len(pwd) > 1:
				pwd.pop()
			else:
				for directory in pwd[-1].children:
					flag = 0
					if directory.dir_name == i:
						flag = 1
						pwd.append(directory)
						break
				if flag == 0:
					print('ERR: INVALID PATH')
		if flag == 1:
			print('SUCC: REACHED')



	elif cmd[0] == 'rm':

		dirs = cmd[1].split('/')

		if cmd[1][0] == '/':
			present = root
			dirs = dirs[1:]
		else:
			present = pwd[-1]

		if len(dirs) == 1:

			flag = 0

			#Used enumerate because we need the index of the element to be able to pop it.
			for index, child in enumerate(present.children):
				if child.dir_name == dirs[0]:
					present.children.pop(index)
					flag = 1
					print('SUCC: DELETED')
					break
			
			if flag == 0:
				print('ERR: INVALID PATH')


		else:
			node = present
			count = 0

			for d in dirs:

				if d == '':
					continue

				found_in_child = False

				for index, child in enumerate(node.children):

				    if child.dir_name == d:
				        found_in_child = True
				        count += 1
				        curr = node
				        node = child

				    if found_in_child and count == len(dirs):
				        curr.children.pop(index)
				        print("SUCC: DELETED")



	elif " ".join(cmd) == 'session clear':
		#new session would mean a clean pwd

		root = Directory('/')
		pwd = [root]

		print('SUCC: CLEARED: RESET TO ROOT')


	else:
		print('ERR: CANNOT RECOGNIZE INPUT.')

